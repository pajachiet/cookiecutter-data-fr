# Cookiecutter Data HAS

_Une structure standard pour initier un projet d'analyse de données en Python à la Haute Autorité de Santé._


### Dépendances nécessaire pour utiliser ce template cookiecutter

 - Python >=3.5
 - [Cookiecutter Python package](http://cookiecutter.readthedocs.org/en/latest/installation.html) >= 1.4.0: 
 Cela peut être installé via `pip` ou par `conda`, selon la façon dont vous gérez vos packages Python:

``` bash
$ pip install cookiecutter
```

ou

``` bash
$ conda config --add channels conda-forge
$ conda install cookiecutter
```


### Pour démarrer un nouveau projet, lancez la commande suivante :

    cookiecutter https://gitlab.com/has-sante/public/cookiecutter-data-has/

ou avec ssh :

    cookiecutter git@gitlab.com:has-sante/public/cookiecutter-data-has/

Ce projet s'inspire fortement de [cookiecutter-data-science](https://github.com/drivendata/cookiecutter-data-science).


### La structure de dossier créée est la suivante
 
```
├── data
│   ├── raw                 <- Les données originale, 
│   ├── interim             <- Les données intérmédiaire transformée
│   ├── clean               <- Les données finale, analysables
│   ├── output              <- Les sorties des modèles
│   ├── schemas             <- Les schémas de données interopérables
|
├── documentation           <- Documentation du projet
|
├── notebooks               <- Notebooks Jupyter
|
├── rapports                <- Rapports d'analyse
│   └── images              <- Images et graphiques
|
├── src                     <- Code source
│   ├── data                <- Code d'acquisition des données
│   ├── preprocessing       <- Code de préparation, nettoyage et extraction de variable
│   ├── analyse             <- Code d'analyse
│   ├── visualisation       <- Code de visualisation
|
├── tests                   <- Tests du projet
|
├── EBT.md                  <- Expression de besoin technique pour prestation externe
├── INSTALL.md              
├── LICENCE.txt
├── README.md
├── canvas.md               <- Canvas de projet data permettant de cadrer le besoin avec les services métiers
├── requirements.txt        <- Fichier de requirements for reproduire l'environnement d'analyse (cf INSTALL.md)
└── setup.py                <- Rend le projet installable via pip (pip install -e .) permettant à src de s'importer
```

## Contribuer

Les contributions sont bienvenues !

# Licence
Licensed under the EUPL-1.2-or-later
