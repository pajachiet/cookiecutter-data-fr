# {{cookiecutter.project_name}} : Expression de Besoin Technique

Dans le cadre d'un marché public relevant d'un accord cardre,l'Expression de Besoin Technique (ou EBT) est adressée aux titulaires (*prestataires*) afin que ceux-ci prennent connaissances du projet et déclare leur **capacité à faire**.

## Projet concerné

{{cookiecutter.description}}

## Description des travaux attendus

### Préparation des sources 

### Points d'attentions attendus

- utiliser des noms de colonnes standards pour des informations communes entre sources
- gérer de façon uniforme les valeurs manquantes
- normaliser et corriger au besoin le contenu de certaines colonnes
- documenter les traitements (textes et schmémas)
- standardiser et documenter le schéma des tables via le standard Table Schema, qui sera utilisé pour valider le format des tables
- uniformiser la documentation des nomenclature (lexique) des variables, dans un format aisément réutilisable informatiquement  (ex : table avec une ligne par [variable, modalité, signification])
- faciliter la mise à jour des données, à partir d'une nouvelle version d'un même fichier source, ou d'un fichier additionnel concernant une nouvelle année de données

Les données intermédiaires ainsi préparées seront diffusées en open data sur data.gouv.fr.

### Livrable principal attendu

### Analyse exploratoire des données

Des analyses exploratoires des données seront réalisées pour en exploiter le potentiel comparatif, dans l'espace et dans le temps. 

La forme au choix : jupyter + plotly, outil de dashboarding python (dash, bokeh), power BI

### Soutien métier


## Considérations techniques

Les travaux seront pilotés par la mission data de la HAS, et s'inscrivent dans le développement des outils socles de la plateforme data de la HAS.

Au-delà des résultats finaux produits, une attention particulière sera donc porté à la qualité des moyens employés, 
et à leur cohérence dans l'ensemble des outils mis en oeuvre à la HAS pour le traitement des données.

Un critère fort de succès sera la capacité de réappropriation rapide par la HAS, qui se matérialisera dans la facilité d'apporter des modifications au processus d'ensemble.

Voici quelques éléments important de ce succès :
- La préparation des sources et la constitution de la base scope-santé devra être entièrement écrite en code, 
du téléchargement initial des sources à leur export via api sur data.gouv.fr, en passant par l'execution des traitements et tests intermédiaires.
- Les traitements se feront en Python, avec la librairie pandas, sauf considération technique éventuelle invitant à utiliser pyspark et des librairies additionnelles.
- Tous les codes et analyses seront versionnés sur le dépôt gitlab de la HAS.
- Les traitements seront effectués sur les postes individuels et/ou sur une machine virtuelle de la HAS mise à disposition. 
- La configuration de l'environnement d'execution des traitements sera reproductible, via un fichier décrivant les dépendances et - si utile - l'usage d'un environnement Docker
- Les tests du format des données seront automatisés via gitlab-ci, via pytest et la librairie goodtables.
- La documentation sera rédigée en Markdown, complémentés d'éventuelles images et de fichiers plus techniques aux formats json et yaml


## Calendrier prévisionnel

- UO nécessaires envisagées :

## Date-limite fixée pour la réponse de capacité à faire

3 semaines après sollicitation 
