Canvas de projet données HAS (inspiré de [canvasML](https://medium.com/louis-dorard/from-data-to-ai-with-the-machine-learning-canvas-part-i-d171b867b047))* : Permet de cadrer les besoins du projet data avec les services métiers.

# Propositions de valeur, {{cookiecutter.project_name}}

- Objectif principal : 
- Type de livrable attendu : 
- Public visé : 
- Echéance : 

# Exploration
## Sources de données

- Source 1 :
    - Format : .*txt, .csv, base sql, ...*
    - Volumétire : *nb documents ie. unités de données, nb lignes, taille en Gb*
    - Structuration : *structurée (tabulaire, json, ...) ou non structurée (textes, images, ...)*
- Source 2 : 
- Source ...

## Système de collecte de la donnée
- Fournisseur : 
- Fréquence :
- Moyen technnique : *envoie mail, addresse web, job gitlab, job talend, ...*
- Accès à la donnée une fois collectée : *serveur de fichiers, poste individuel...*

## Pré-traitement de la donnée

- Pré-traitement : *nettoyage, reformattage, pseudonymisation...*
- Processus d'extraction de features (optionnel) : 
- Langages et librairies : 

# Analyse et production

## Analyse et algorithme

- Entrées / Sorties :
- Produits complémentaires : *sorties secondaires, ex: Graphiques, tableaux de résumés, ...*
- Tâche d'apprentissage (optionnel) : *clustering, classification, regression, ...*
- Algorithme (optionnel) : *envisagé ou utilisé*
- Langages et librairies : 

## Temporalité de l'analyse

- Fréquence de MAJ de l'analyse : 
- Temps nécessaire : 
    - humain : 
    - machine :

## Décisions 

Actions déclenchées par les résultats obtenus par l'analyse et/ou l'algorithme : 

## Evaluation des résultats de l'analyse

Métriques et méthodes pour évaluer les résultats de l'analyse.

## Temporalité du service (optionnel)
- Contexte de production : 
- Fréquence de mise en oeuvre :
- Contraintes de performance : *résultats rapides nécessaires*

# Evaluation du projet

Métriques et méthodes d'évaluation du projet final.
