# {{cookiecutter.project_name}}

{{cookiecutter.description}}

Les données proviennent de [nom_source](url).

Pour installer le projet, lire le fichier [INSTALL.md](INSTALL.md).

Le README.md du template cookiecutter explique [l'organisation](https://gitlab.com/has-sante/public/cookiecutter-data-has#la-structure-de-dossier-cr%C3%A9%C3%A9e-est-la-suivante) des dossiers

% if cookiecutter.open_source_license == 'EUPLv1.2' %}
Licensed under the EUPL-1.2-or-later
{% endif %}
