# Données

Ce dossier contient les données du projet. 

Nous ne versionnons pas les données brutes d'entrées sauf execption (petit volume et sans contrainte d'accès).

Nous ne versionnons pas les données nettoyées. Nous pensons en effet qu'elles doivent pouvoir être générées automatiquement par le code.