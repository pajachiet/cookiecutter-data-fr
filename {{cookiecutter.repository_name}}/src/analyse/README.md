Ce dossier contient les modules et fonctions nécessaires à :
- l'analyse des données : algorithmes, modèles, utilitaires variés pour faciliter l'analyse.
  
Les résultats de ces scripts vont typiquement dans les dossiers `rapports` ou `data/output`.