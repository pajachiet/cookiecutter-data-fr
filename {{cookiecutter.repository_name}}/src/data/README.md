Ce dossier contient les modules et fonctions nécessaires à :
- l'acquisition des données dans des formats adaptés à l'analyse. 
- la transformation de ces données brutes en des formats intérmédiaires

Les résultats de ces scripts vont typiquement dans les dossiers `data/raw` ou `data/interim`.