Ce dossier contient les modules et fonctions nécessaires à :
- la visualisation des données
- la production de graphiques 

Les résultats de ces scripts vont typiquement dans le dossier `rapports/images`.