Ce dossier contient les modules et fonctions nécessaires à :
- la transformation et le nettoyage des données avant analyse,
- la production de schemas de données, 
- la production de features pour un modèle d'apprentissage statistique

Les résultats de ces scripts vont typiquement dans le dossier `data/clean`.