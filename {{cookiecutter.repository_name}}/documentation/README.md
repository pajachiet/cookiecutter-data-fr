La documentation comprend: 
 - les dictionnaires de données originaux, souvent édités par les producteurs mais non formalisés dans des schémas interopérables
 - les documentations aux formats variées que l'on peut récolter sur le projet.